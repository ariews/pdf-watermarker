<?php

namespace Uvinum\PDFWatermark;

interface WatermarkInterface
{
    /**
     * Return the path to the tmp file
     *
     * @return string
     */
    public function getFilePath(): string;

    /**
     * Returns the watermark's height
     *
     * @return int
     */
    public function getHeight(): int;

    /**
     * Returns the watermark's width
     *
     * @return int
     */
    public function getWidth(): int;
}